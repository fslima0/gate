<%@ taglib uri="http://www.gate.com.br/gate" prefix="G"%>

<G:template filename="/WEB-INF/views/gateconsole/Demo/Main.jsp">
	<div>	
		<div>
			<pre class="language-markup"><code><!--
									<fieldset>
										<label>
											Date:
											<span>
												<input class="Date"/>
											</span>
										</label>
									</fieldset>
								--></code></pre>
		</div>
		<div>
			<fieldset>
				<label>
					Date:
					<span>
						<input class="Date"/>
					</span>
				</label>
			</fieldset>
		</div>
	</div>
	<div>
		<div>
			<pre class="language-markup"><code><!--
									<fieldset>
										<label>
											Time:
											<span>
												<input class="Time"/>
											</span>
										</label>
									</fieldset>
								--></code></pre>
		</div>
		<div>
			<fieldset>
				<label>
					Time:
					<span>
						<input class="Time"/>
					</span>
				</label>
			</fieldset>
		</div>
	</div>
	<div>
		<div>
			<pre class="language-markup"><code><!--
									<fieldset>
										<label>
											Month:
											<span>
												<input class="Month"/>
											</span>
										</label>
									</fieldset>
								--></code></pre>
		</div>
		<div>
			<fieldset>
				<label>
					Month:
					<span>
						<input class="Month"/>
					</span>
				</label>
			</fieldset>
		</div>
	</div>
	<div>
		<div>
			<pre class="language-markup"><code><!--
									<fieldset>
										<label>
											Date Time
											<span>
												<input class="DateTime"/>
											</span>
										</label>
									</fieldset>
								--></code></pre>
		</div>
		<div>
			<fieldset>
				<label>
					Date Time
					<span>
						<input class="DateTime"/>
					</span>
				</label>
			</fieldset>
		</div>
	</div>
	<div>
		<div>
			<pre class="language-markup"><code><!--
									<fieldset>
										<label>
											Date Interval:
											<span>
												<input class="DateInterval"/>
											</span>
										</label>
									</fieldset>
								--></code></pre>
		</div>
		<div>
			<fieldset>
				<label>
					Date Interval:
					<span>
						<input class="DateInterval"/>
					</span>
				</label>
			</fieldset>
		</div>
	</div>
	<div>
		<div>
			<pre class="language-markup"><code><!--
									<fieldset>
										<label>
											Time Interval:
											<span>
												<input class="TimeInterval"/>
											</span>
										</label>
									</fieldset>
								--></code></pre>
		</div>
		<div>
			<fieldset>
				<label>
					Time Interval:
					<span>
						<input class="TimeInterval"/>
					</span>
				</label>
			</fieldset>
		</div>
	</div>
	<div>
		<div>
			<pre class="language-markup"><code><!--
									<fieldset>
										<label>
											Month Interval:
											<span>
												<input class="MonthInterval"/>
											</span>
										</label>
									</fieldset>
								--></code></pre>
		</div>
		<div>
			<fieldset>
				<label>
					Month Interval:
					<span>
						<input class="MonthInterval"/>
					</span>
				</label>
			</fieldset>
		</div>
	</div>
	<div>
		<div>
			<pre class="language-markup"><code><!--
									<fieldset>
										<label>
											Date Time Interval:
											<span>
												<input class="DateTimeInterval"/>
											</span>
										</label>
									</fieldset>
								--></code></pre>
		</div>
		<div>
			<fieldset>
				<label>
					Date Time Interval:
					<span>
						<input class="DateTimeInterval"/>
					</span>
				</label>
			</fieldset>
		</div>
	</div>

	<div>
		<div>
			<pre class="language-markup"><code><!--
									<fieldset>
										<label>
											Icon:
											<span>
												<input class="Icon"/>
											</span>
										</label>
									</fieldset>
								--></code></pre>
		</div>
		<div>
			<fieldset>
				<label>
					Icon:
					<span>
						<input class="Icon"/>
					</span>
				</label>
			</fieldset>
		</div>
	</div>
</G:template>