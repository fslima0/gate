<%@ taglib uri="http://www.gate.com.br/gate" prefix="g"%>

<g:template filename="/WEB-INF/views/MAIN.jsp">
	<main>
		<section>
			<g:alert/>
			<g:insert/>
		</section>
	</main>
</g:template>