<%@ taglib uri="http://www.gate.com.br/gate" prefix="G"%>

<G:template filename="/WEB-INF/views/gateconsole/Demo/Main.jsp">
	<div style="grid-column: 1 / span 2">
		<div>
			<pre class="language-markup"><code><!--
			<fieldset style="height: 400px">
				<g-text-editor name="g-text-editor" value="Hello World!!!">
				</g-text-editor>
			</fieldset>	
								--></code></pre>
		</div>
		<div>
			<fieldset style="height: 400px">
				<g-text-editor name="g-text-editor" value="Hello World!!!">
				</g-text-editor>
			</fieldset>
		</div>
	</div>
</G:template>